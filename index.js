const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function addKata(title, text) {
    let header = document.createElement('div');
    header.textContent = title;
    document.body.appendChild(header);

    let kataEle = document.createElement('div');
    kataEle.textContent = JSON.stringify(text);
    document.body.appendChild(kataEle);
}

// Write a function that returns an array with the cities in 'gotCitiesCSV'. Remember to also append the results to the page.
function Kata1() {
    let citiesArr = gotCitiesCSV.split(",");
    addKata("Kata 1", citiesArr);
    return citiesArr;
}

// Write a function that returns an array of words from the sentence in 'bestThing'. Remember to also append the results to the page.
function Kata2() {
    let wordsArr = bestThing.split(" ");
    addKata("Kata 2", wordsArr);
    return wordsArr;
}

// Write a function that returns a string separated by semi-colons instead of commas from 'gotCitiesCSV'. Remember to also append the results to the page.
function Kata3() {
    let citiesSemiColons = gotCitiesCSV.split(",").join(";");
    addKata("Kata 3", citiesSemiColons);
    return citiesSemiColons;
}

// Write a function that returns a CSV (comma-separated) string from the 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata4() {
    let lotrCitiesCSV = lotrCitiesArray.join(",");
    addKata("Kata 4", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that returns an array with the first 5 cities in 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata5() {
    let firstFiveCities = lotrCitiesArray.slice(0, 5);
    addKata("Kata 5", firstFiveCities);
    return firstFiveCities;
}

// Write a function that returns an array with the last 5 cities in 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata6() {
    let lastFiveCities = lotrCitiesArray.slice(-5);
    addKata("Kata 6", lastFiveCities)
    return lastFiveCities;
}

// Write a function that returns an array with the 3rd to 5th city in 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata7() {
    let thirdToFifthCity = lotrCitiesArray.slice(2, 5);
    addKata("Kata 7", thirdToFifthCity);
    return thirdToFifthCity;
}

// Write a function that uses 'splice' to remove 'Rohan' from 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata8() {
    lotrCitiesArray.splice(2, 1);
    addKata("Kata 8", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'splice' to remove all cities after 'Dead Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata9() {
    lotrCitiesArray.splice(5, 2);
    addKata("Kata 9", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'splice' to add 'Rohan' back to 'lotrCitiesArray' right after 'Gondor' and returns the new modified 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata10() {
    lotrCitiesArray.splice(2, 0, "Rohan");
    addKata("Kata 10", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'splice' to rename 'Dead Marshes' to 'Deadest Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. Remember to also append the results to the page.
function Kata11() {
    lotrCitiesArray.splice(5, 1, "Deadest Marshes")
    addKata("Kata 11", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'slice' to return a string with the first 14 characters from 'bestThing'. Remember to also append the results to the page.
function Kata12() {
    let modifiedBestThing = bestThing.slice(0, 14);
    addKata("Kata 12", modifiedBestThing);
    return modifiedBestThing;
}

// Write a function that uses 'slice' to return a string with the last 12 characters from 'bestThing'. Remember to also append the results to the page.
function Kata13() {
    let modifiedBestThing = bestThing.slice(-12);
    addKata("Kata 13", modifiedBestThing);
    return modifiedBestThing;
}

// Write a function that uses 'slice' to return a string with the characters between the 23rd and 38th position of 'bestThing' (i.e., 'boolean is even'). 
function Kata14() {
    let modifiedBestThing = bestThing.slice(23, 39);
    addKata("Kata 14", modifiedBestThing);
    return modifiedBestThing;
}

// Write a function that does the exact same thing as #13 but use the 'substring' method instead of 'slice'.
function Kata15() {
    let modifiedBestThing = bestThing.substr(-12);
    addKata("Kata 15", modifiedBestThing);
    return modifiedBestThing;
}

// Write a function that does the exact same thing as #14 but use the 'substring' method instead of 'slice'.
function Kata16() {
    let modifiedBestThing = bestThing.substr(23, 15);
    addKata("Kata 16", modifiedBestThing);
    return modifiedBestThing;
}

// Write a function that finds and returns the index of 'only' in 'bestThing'.
function Kata17() {
    let index = bestThing.indexOf("only");
    addKata("Kata 17", index);
    return index;
}

// Write a function that finds and returns the index of the last word in 'bestThing'.
function Kata18() {
    let lastWord = bestThing.split(" ");
    lastWord = lastWord[lastWord.length - 1];
    
    let index = bestThing.indexOf(lastWord);
    addKata("Kata 18", index);
    return index;
}

// Write a function that finds and returns an array of all cities from 'gotCitiesCSV' that use double vowels ('aa', 'ee', etc.).
function Kata19() {
    let doubleVowels = ["aa", "ee", "ii", "oo", "uu"];
    let dvWords = gotCitiesCSV.toLowerCase().split(",").reduce( (pre, cur) => {
        let dvs = doubleVowels.filter(v => cur.indexOf(v) !== -1)
        if(dvs.length !== 0) pre.push(cur);

        return pre;
    }, []);

    addKata("Kata 19", dvWords);
    return dvWords;
}

// Write a function that finds and returns an array with all cities from 'lotrCitiesArray' that end with 'or'.
function Kata20() {
    let orLotrCities = lotrCitiesArray.reduce( (pre, cur) => {
        if(cur.substr(-2) === "or") pre.push(cur);
        return pre;
    }, []);

    addKata("Kata 20", )
}

// Write a function that finds and returns an array with all the words in 'bestThing' that start with a 'b'.
function Kata21() {
    let modifiedBestThing = bestThing.split(" ")
        .filter(w => w.indexOf("b") !== -1);
    
    addKata("Kata 21", modifiedBestThing);
    return modifiedBestThing
}

// Write a function that returns 'Yes' or 'No' if 'lotrCitiesArray' includes 'Mirkwood'.
function Kata22() {
    let yesOrNo = "No";
    for(let i = 0; i < lotrCitiesArray.length; ++i)
        if(lotrCitiesArray[i] === "Mirkwood")
            yesOrNo = "Yes"

    addKata("Kata 22", yesOrNo);
    return yesOrNo;
}

// Write a function that returns 'Yes' or 'No' if 'lotrCitiesArray' includes 'Hollywood'. Remember to also append the results to the page.
function Kata23() {
    let yesOrNo = "No";
    for(let i = 0; i < lotrCitiesArray.length; ++i)
        if(lotrCitiesArray[i] === "Hollywood")
            yesOrNo = "Yes"

    addKata("Kata 23", yesOrNo);
    return yesOrNo;
}

// Write a function that returns the index of 'Mirkwood' in 'lotrCitiesArray'.
function Kata24() {
    let mirkdex = lotrCitiesArray.indexOf("Mirkwood");
    addKata("Kata 24", mirkdex);
    return mirkdex;
}

// Write a function that finds and returns the first city in 'lotrCitiesArray' that has more than one word.
function Kata25() {
    let modifiedLotrCities = lotrCitiesArray
        .filter( c => c.split(" ").length > 1)[0];

    addKata("Kata 25", modifiedLotrCities);
    return modifiedLotrCities;

}

// Write a function that reverses the order of 'lotrCitiesArray' and returns the new array.
function Kata26() {
    lotrCitiesArray.reverse();
    
    addKata("Kata 26", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that sorts 'lotrCitiesArray' alphabetically and returns the new array.
function Kata27() {
    lotrCitiesArray.sort();

    addKata("Kata 27", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that sorts 'lotrCitiesArray' by the number of characters in each city (i.e., shortest city names go first) and return the new array.
function Kata28() {
    lotrCitiesArray.sort((a, b) => {
        // returns < 0 a comes before b
        // returns 0 leaves a and b alone in relation to each other
        // returns > 0 b comes before a

        return a.length - b.length;
    })

    addKata("Kata 28", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'pop' to remove the last city from 'lotrCitiesArray' and returns the new array.
function Kata29() {
    lotrCitiesArray.pop();
    addKata("Kata 29", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'push' to add back the city from 'lotrCitiesArray' that was removed in #29 to the back of the array and returns the new array
function Kata30() {
    lotrCitiesArray.push("Deadest Marshes");
    addKata("Kata 30", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'shift' to remove the first city from 'lotrCitiesArray' and returns the new array.
function Kata31() {
    lotrCitiesArray.shift();
    addKata("Kata 31", lotrCitiesArray);
    return lotrCitiesArray;
}

// Write a function that uses 'unshift' to add back the city from 'lotrCitiesArray' that was removed in #31 to the front of the array and returns the new array. 
function Kata32() {
    lotrCitiesArray.unshift("Rohan");
    addKata("Kata 32", lotrCitiesArray);
    return lotrCitiesArray;
}

function runKatas() {
    Kata1();
    Kata2();

    Kata3();
    Kata4();
    Kata5();
    Kata6();
    Kata7();
    Kata8();
    Kata9();
    Kata10();
    Kata11();
    Kata12();

    Kata13();
    Kata14();
    Kata15();
    Kata16();
    Kata17();
    Kata18();
    Kata19();
    Kata20();
    Kata21();
    Kata22();

    Kata23();
    Kata24();
    Kata25();
    Kata26();
    Kata27();
    Kata28();
    Kata29();
    Kata30();
    Kata31();
    Kata32();
}

runKatas();